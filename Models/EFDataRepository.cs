﻿using EnviroC.Models.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace EnviroC.Models
{
    public class EFDataRepository : IErrandsRepository
    {

        private ApplicationDbContext context;

        public EFDataRepository(ApplicationDbContext c)
        {
            context = c;
        }

        public IQueryable<Errand> Errands => context.Errands.Include(errand=>errand.Samples).Include(errand=>errand.Pictures);
        public IQueryable<Department> Departments => context.Departments;
        public IQueryable<ErrandStatus> ErrandStatus1 => context.ErrandStatus1;
        public IQueryable<Employee> Employees => context.Employees;

        

        public Task<Errand> GetErrand(int id)
        {
            return Task.Run(() =>
             {
                // Looks at the input Id (a string, not an int) and compares it with those in the fake database. Returns a Task containing the Errand object corresponding to that Id.

                var errandDetail = Errands.Where(td => td.ErrandId == id).FirstOrDefault();
                 return errandDetail;
             });

        }

        //Create and Update
        public void SaveErrand(Errand errand)
        {
            if (errand.ErrandId == 0)
            {
                //If the errand does not exist yet, look for a Sequence object where id = 1 (it's the only one)
                var seq = context.Sequences.Where(s => s.Id == 1).FirstOrDefault();

                errand.RefNumber = "2020-45-" + (seq.CurrentValue).ToString();

                // Add some default data
                errand.StatusId = "S_A";
                errand.EmployeeId = "";
                errand.DepartmentId = "";
                errand.InvestigatorAction = "";
                errand.InvestigatorInfo = "";

                // When no fields equal null, add the errand to the database
                context.Errands.Add(errand);

                //Now add one to the CurrentValue counter
                seq.CurrentValue++;
            }
            else
            {
                // If the Errand exists
                Errand dbEntry = context.Errands.FirstOrDefault(e1 => e1.ErrandId == errand.ErrandId);
                if (dbEntry != null)
                {
                    // If the Errand exists and is already in the database
                    int oldErrandId = errand.ErrandId;
                    dbEntry = errand;
                    dbEntry.ErrandId = oldErrandId;
                    //Now all data in the Errand object has been replaced except ErrandId
                }
            }
            context.SaveChanges();
        }

        //Delete
        public Errand DeleteErrand(int ErrandId)
        {
            Errand dbEntry = context.Errands.FirstOrDefault(e1 => e1.ErrandId == ErrandId);
            if (dbEntry != null)
            {                    
                // If the Errand exists and is already in the database
                context.Errands.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
