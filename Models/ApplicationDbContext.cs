﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using EnviroC.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EnviroC.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        
            //DbSet-egenskaper
            public DbSet<Errand> Errands { get; set; }
            public DbSet<Department> Departments { get; set; }
            public DbSet<Employee> Employees { get; set; }

        internal Task<IErrandsRepository> AddAsync()
        {
            throw new NotImplementedException();
        }

        public DbSet<ErrandStatus> ErrandStatus1 { get; set; }
            public DbSet<Picture> Pictures { get; set; }
            public DbSet<Sample> Samples { get; set; }
            public DbSet<Sequence> Sequences { get; set; }
    } 
    
}

   


