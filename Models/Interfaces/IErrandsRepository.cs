﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models.Interfaces
{
    // This interface makes it so that the database class that is entered has the correct content.
    public interface IErrandsRepository
    {
        //Read
        IQueryable<Errand> Errands { get; }
        IQueryable<Department> Departments { get; }
        IQueryable<ErrandStatus> ErrandStatus1 { get; }
        IQueryable<Employee> Employees { get; }

        Task<Errand> GetErrand(int id);
        //Create and Update
        void SaveErrand(Errand errand);
        //Delete
        Errand DeleteErrand(int ErrandId);



    }


}
