﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models
{
    public class ErrandStatus
    {
        [Key]
        public String StatusId { set; get; }
        public String StatusName { set; get; }
    }
}
