﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models
{
    public class Errand
    {
        public int ErrandId { set; get; }
        public String RefNumber { set; get; }
        [Required(ErrorMessage ="Detta fält är obligatoriskt.")]
        public String Place { set; get; }
        [Required(ErrorMessage = "Detta fält är obligatoriskt.")]
        public String TypeOfCrime { set; get; }
        [Required(ErrorMessage = "Detta fält är obligatoriskt.")]

        public DateTime DateOfObservation { set; get; }
        public String Observation { set; get; }
        public String InvestigatorInfo { set; get; }
        public String InvestigatorAction { set; get; }
        [Required(ErrorMessage = "Detta fält är obligatoriskt.")]
        public String InformerName { set; get; }
        [Required(ErrorMessage = "Detta fält är obligatoriskt.")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Skriv bara siffror.")]
        public String InformerPhone { set; get; }
        public String StatusId { set; get; }
        public String DepartmentId { set; get; }
        public String EmployeeId { set; get; }

        public ICollection<Sample> Samples { get; set; }
        public ICollection<Picture> Pictures { get; set; }
        

    }
}
