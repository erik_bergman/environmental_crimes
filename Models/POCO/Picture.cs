﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models
{
    public class Picture
    {
        public int PictureId { set; get; }
        public String PictureName { set; get; }
        public int ErrandId { set; get; }
    }
}
