﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models
{
    public class Sample
    {
        public int SampleId { set; get; }
        public String SampleName { set; get; }
        public int ErrandId { set; get; }
    }
}
