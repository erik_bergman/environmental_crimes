﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Du måste fylla i ett användarnamn")]
        [Display(Name = "Användarnamn:")] //If not filled in, 'UserName' will appear
        public string UserName { get; set; }
        [Required(ErrorMessage = "Du måste fylla i ett lösenord")]
        [Display(Name = "Lösenord:")]
        [UIHint("password")] // Makes the characters invisible
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
    }
}
