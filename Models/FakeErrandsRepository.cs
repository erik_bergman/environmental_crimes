﻿using EnviroC.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models
{
    public class FakeErrandsRepository
    {

        public IQueryable<Errand> Errands => new List<Errand>
        {
            new Errand{ ErrandId = 1, Place = "Skogslunden vid Jensens gård", TypeOfCrime="Sopor", DateOfObservation = new DateTime(2020,04,24), Observation ="Anmälaren var på promeand i skogslunden när hon upptäckte soporna", InvestigatorInfo = "Undersökning har gjorts och bland soporna hittades bl.a ett brev till Gösta Olsson", InvestigatorAction = "Brev har skickats till Gösta Olsson om soporna och anmälan har gjorts till polisen 2018-05-01", InformerName = "Ada Bengtsson", InformerPhone = "0432-5545522", StatusId="Klar", DepartmentId="Renhållning och avfall", EmployeeId ="Susanne Fred"},new Errand{ ErrandId = 2, Place = "Småstadsjön", TypeOfCrime="Oljeutsläpp", DateOfObservation = new DateTime(2020,04,29), Observation ="Jag såg en oljefläck på vattnet när jag var där för att fiska", InvestigatorInfo = "Undersökning har gjorts på plats, ingen fläck har hittas", InvestigatorAction = "", InformerName = "Bengt Svensson", InformerPhone = "0432-5152255", StatusId="Ingen åtgärd", DepartmentId="Natur och Skogsvård", EmployeeId ="Oskar Ivarsson"},new Errand{ ErrandId = 3, Place = "Ödehuset", TypeOfCrime="Skrot", DateOfObservation = new DateTime(2020,05,02), Observation ="Anmälaren körde förbi ödehuset och upptäcker ett antal bilar och annat skrot", InvestigatorInfo = "Undersökning har gjorts och bilder har tagits", InvestigatorAction = "", InformerName = "Olle Pettersson", InformerPhone = "0432-5255522", StatusId="Påbörjad", DepartmentId="Miljö och Hälsoskydd", EmployeeId ="Lena Larsson"},new Errand{ ErrandId = 4, Place = "Restaurang Krögaren", TypeOfCrime="Buller", DateOfObservation = new DateTime(2020,06,04), Observation ="Restaurangen hade för högt ljud på så man inte kunde sova", InvestigatorInfo = "Bullermätning har gjorts. Man håller sig inom riktvärden", InvestigatorAction = "Meddelat restaurangen att tänka på ljudet i fortsättning", InformerName = "Roland Jönsson", InformerPhone = "0432-5322255", StatusId="Klar", DepartmentId="Miljö och Hälsokydd", EmployeeId ="Martin Kvist"},new Errand{ ErrandId = 5, Place = "Torget", TypeOfCrime="Klotter", DateOfObservation = new DateTime(2020,07,10), Observation ="Samtliga skräpkorgar och bänkar är nedklottrade", InvestigatorInfo = "", InvestigatorAction = "", InformerName = "Peter Svensson", InformerPhone = "0432-5322555", StatusId="Inrapporterad", DepartmentId="Ej tillsatt", EmployeeId ="Ej tillsatt"}
        }.AsQueryable<Errand>();

        public Task<Errand> GetErrand(int id)
        {
            return Task.Run(() => {
                // Looks at the input Id (a string, not an int) and compares it with those in the fake database. Returns a Task containing the Errand object corresponding to that Id.

                var errandDetail = Errands.Where(td => td.ErrandId == id).FirstOrDefault();
                return errandDetail;
            });

        }

        public Task<List<Errand>> GetErrands()
        {
            return Task.Run(() => {
                var errands = Errands.ToList();
                return errands;
            });
        }

    }
}
