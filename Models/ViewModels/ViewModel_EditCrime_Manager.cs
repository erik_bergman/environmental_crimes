﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models.ViewModels
{
    public class ViewModel_EditCrime_Manager
    {
        // This class exists to handle the input on the LoggedIn_Crime_Manager page.
        public bool NoActionBool { set; get; }
        public string Comment { set; get; }
        public string EmployeeId { set; get; }
    }
}
