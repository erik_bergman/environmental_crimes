﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnviroC.Models.ViewModels
{
    public class ViewModel_EditCrime_Investigator
    {
        public string StatusId { set; get; }
        public string InvestigatorAction { set; get; }
        public string InvestigatorInfo { set; get; }

        public IFormFile UploadedSample { set; get; }
        public IFormFile UploadedImage { set; get; }
        public IEnumerable<Sample> SampleList { set; get; }
        public IEnumerable<Picture> PictureList { set; get; }
    }
}
