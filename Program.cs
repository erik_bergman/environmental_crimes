using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace EnviroC
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // .Run() was previously used here
            var host = CreateHostBuilder(args).Build();
            InitializeDatabase(host);
        }

        private static void InitializeDatabase(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                { 
                    //DBInit is the code that should be converted into a database
                    DBInit.EnsurePopulated(services);
                    IdentityInit.EnsurePopulated(services).Wait();
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "Det gick inte att fylla databasen");
                }
                host.Run();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
