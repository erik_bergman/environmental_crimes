﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace EnviroC.Controllers
{
    public class HomeController : Controller
    {
        private IErrandsRepository repository;
        public HomeController(IErrandsRepository repo)
        {
            repository = repo;

        }

        public ViewResult Index()
        {

            ViewBag.Title = "Småstads kommun";
            return View();
        }
        public ViewResult Login(string returnUrl)
        {
            ViewBag.Title = "Småstads kommun - Inloggning";
            return View(new LoginModel
            {
                ReturnUrl = returnUrl
            }); ;
        }

    }
}
