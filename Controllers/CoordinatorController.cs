﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.Interfaces;
using EnviroC.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EnviroC.Controllers
{
    [Authorize]
    public class CoordinatorController : Controller
    {

        private IErrandsRepository repository;
        public CoordinatorController(IErrandsRepository repo)
        {
            repository = repo;
        }
        [Authorize(Roles = "Coordinator")]
        public ViewResult LoggedIn_Coordinator()
        {
            ViewBag.Title = "Småstads kommun - Samordnare";

            string username = TempData.Peek("username").ToString(); IQueryable<ViewModel_MyErrand> relevantErrands = null;

            Employee thisEmployee = repository.Employees.Where(em => em.EmployeeId == username).FirstOrDefault();
            Department thisDepartment = repository.Departments.Where(em => em.DepartmentId == thisEmployee.DepartmentId).FirstOrDefault();

            relevantErrands = from err in repository.Errands
                             join stat in repository.ErrandStatus1 on err.StatusId equals stat.StatusId
                             join dep in repository.Departments on err.DepartmentId equals dep.DepartmentId into departmentErrand
                             from deptE in departmentErrand.DefaultIfEmpty()
                             join em in repository.Employees on err.EmployeeId equals em.EmployeeId into employeeErrand
                             from empE in employeeErrand.DefaultIfEmpty()
                             orderby err.RefNumber descending
                             select new ViewModel_MyErrand
                             {
                                 DateOfObservation = err.DateOfObservation,
                                 ErrandId = err.ErrandId,
                                 RefNumber = err.RefNumber,
                                 TypeOfCrime = err.TypeOfCrime,
                                 StatusName = stat.StatusName,
                                 DepartmentName = (err.DepartmentId == null ? "Ej tillsatt" : deptE.DepartmentName),
                                 EmployeeName = (err.EmployeeId == null ? "Ej tillsatt" : empE.EmployeeName)
                             };


            ViewBag.thisDepartment = thisDepartment;
            ViewBag.thisEmployee = thisEmployee;
            ViewBag.relevantDepartments = repository.Departments;
            ViewBag.relevantEmployees = repository.Employees;
            ViewBag.relevantErrandStatus1 = repository.ErrandStatus1;
            return View(relevantErrands);
        }
        [Authorize(Roles = "Coordinator")]
        public ViewResult LoggedIn_ReportCrime_Coordinator()
        {
            ViewBag.Title = "Småstads kommun - Samordnare";
            Errand errand = new Errand();

            // Deserialize the saved Json object
            try
            {
                errand = JsonConvert.DeserializeObject<Errand>(TempData ["eTemp"].ToString());
            }
            catch
            {
                Console.WriteLine("Deserialization failed");
                // Set starting value so that the time isn't set to year 0000. This workaround is not needed for the Index page, since it either receives no Errand at all, or it receives TempData from the Validate page.
                errand.DateOfObservation = DateTime.Today;
            }

            // Uses a view from the Home folder.
            return View(errand);
        }

        [Authorize(Roles = "Coordinator")]
        [HttpPost]
        public ViewResult LoggedIn_Validate_Coordinator(Errand errand)
        {
            ViewBag.Title = "Småstads kommun - Samordnare";

            // Serialize the Errand object, turning it into a JSON string. This is done because TempData only allows simple data types.
            string eAsJason = JsonConvert.SerializeObject(errand, Formatting.Indented);
            TempData["eTemp"] = eAsJason;

            return View(errand);
        }
        [Authorize(Roles = "Coordinator")]
        public ViewResult LoggedIn_Thanks_Coordinator()
        {
            ViewBag.Title = "Småstads kommun - Samordnare";
            Errand errand = new Errand();

            // Deserialize the saved Json object
            try
            {
                errand = JsonConvert.DeserializeObject<Errand>(TempData["eTemp"].ToString());
            }
            catch
            {
                Console.WriteLine("Deserialization failed");
            }

            repository.SaveErrand(errand);

            // The object is also sent to Thanks.cshtml so that Model.RefNumber can be displayed
            return View(errand);
        }
        [Authorize(Roles = "Coordinator")]
        public ViewResult LoggedIn_Crime_Coordinator(int id)
        {
            // This method is called from several other classes
            ViewBag.Title = $"Småstads kommun - Ärende {id}";
            ViewBag.Id = id; //Very important, so that the correct input parameter is handed to InvokeAsync() in ShowOneErrand.cs
            TempData["id"] = id;
            ViewBag.DepartmentList = repository.Departments;
            return View();
        }

        [Authorize(Roles = "Coordinator")]
        public IActionResult UpdateDepartment(int errandId, string departmentId)
        {
            Errand errand = repository.GetErrand(errandId).Result;
            errand.DepartmentId = departmentId;
            repository.SaveErrand(errand);
            // Use TempData created in the LoggedIn_Crime method use it as a parameter to return to the same page
            return RedirectToAction("LoggedIn_Crime_Coordinator",new { id = TempData["id"] });
        }

    }
}
