﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EnviroC.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<IdentityUser> UserManager;
        private SignInManager<IdentityUser> SignInManager;
        private IErrandsRepository repository;
        public AccountController(IErrandsRepository repo, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            repository = repo;
            UserManager = userManager;
            SignInManager = signInManager;
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken] // Protects against cross site request forgery
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            IdentityUser user = await UserManager.FindByNameAsync(loginModel.UserName);

            if (ModelState.IsValid)
            {
                // If != null, it means that the UserName exists
                if (user != null)
                {
                    // If the user is already signed in, restart their session
                    await SignInManager.SignOutAsync();
                    // Gather details for creating a link to the correct page
                    Employee loggedInEmployee = repository.Employees.Where(emp => emp.EmployeeId.Equals(user.NormalizedUserName)).FirstOrDefault();
                    String loginPage = $"/{loggedInEmployee.RoleTitle}/LoggedIn_{loggedInEmployee.RoleTitle}";

                    // The username is saved to be used in the LoggedIn methods
                    TempData["username"] = user.UserName;

                    // loginModel.Password is the password entered by the user-
                    // The first boolean is false because the authenticator cookie should *not* be saved
                    // The second boolean is false beacuse the user is allowed repeated attempts to enter the password
                    if ((await SignInManager.PasswordSignInAsync(user, loginModel.Password, false, false)).Succeeded)
                    {
                        return Redirect(loginPage);
                    }
                }
            }
            ModelState.AddModelError("", "Felaktigt användarnamn eller lösenord");

            // The error message is stored inside loginModel
            return View("../Home/Login",loginModel);
        }

        [AllowAnonymous]
        public async Task<RedirectResult> Logout(String returnurl = "/")
        {
            TempData.Remove("username");
            await SignInManager.SignOutAsync();
            return Redirect(returnurl);
        }
    }
}
