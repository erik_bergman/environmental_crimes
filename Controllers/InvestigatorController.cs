﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.Interfaces;
using EnviroC.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EnviroC.Controllers
{
    public class InvestigatorController : Controller
    {
        private IErrandsRepository repository;
        private IWebHostEnvironment environment;


        public InvestigatorController(IErrandsRepository repo, IWebHostEnvironment env)
        {
            repository = repo;
            environment = env;
        }

        [Authorize(Roles = "Investigator")]
        public ViewResult LoggedIn_Investigator()
        {
            ViewBag.Title = "Småstads kommun - Handläggare";
            string username = TempData.Peek("username").ToString();
            IQueryable relevantErrands = null;

            Employee thisEmployee = repository.Employees.Where(em => em.EmployeeId == username).FirstOrDefault();
            Department thisDepartment = repository.Departments.Where(em => em.DepartmentId == thisEmployee.DepartmentId).FirstOrDefault();

            relevantErrands = from err in repository.Errands
                             where err.EmployeeId == thisEmployee.EmployeeId
                             join stat in repository.ErrandStatus1 on err.StatusId equals stat.StatusId
                             join dep in repository.Departments on err.DepartmentId equals dep.DepartmentId into departmentErrand
                             from deptE in departmentErrand.DefaultIfEmpty()

                             join em in repository.Employees on err.EmployeeId equals em.EmployeeId into employeeErrand
                             from empE in employeeErrand.DefaultIfEmpty()
                             orderby err.RefNumber descending
                             select new ViewModel_MyErrand
                             {
                                 DateOfObservation = err.DateOfObservation,
                                 ErrandId = err.ErrandId,
                                 RefNumber = err.RefNumber,
                                 TypeOfCrime = err.TypeOfCrime,
                                 StatusName = stat.StatusName,
                                 DepartmentName = (err.DepartmentId == null ? "Ej tillsatt" : deptE.DepartmentName),
                                 EmployeeName = (err.EmployeeId == null ? "Ej tillsatt" : empE.EmployeeName)
                             };

            ViewBag.thisDepartment = thisDepartment;
            ViewBag.thisEmployee = thisEmployee;
            ViewBag.relevantDepartments = repository.Departments;
            ViewBag.relevantEmployees = repository.Employees;
            ViewBag.relevantErrandStatus1 = repository.ErrandStatus1;
            return View(relevantErrands);
        }
        [Authorize(Roles = "Investigator")]
        public ViewResult LoggedIn_Crime_Investigator(int id)
        {
            // This method is called from several other classes

            ViewBag.Title = $"Småstads kommun - Ärende {id}";
            ViewBag.Id = id; //Very important, so that the correct input parameter is handed to InvokeAsync() in ShowOneErrand.cs
            TempData["id"] = id;
            ViewBag.StatusList = repository.ErrandStatus1;
            return View();
        }
        public async Task<IActionResult> UpdateStatusAsync(int errandId, string statusId, string investigatorAction, string investigatorInfo, IFormFile uploadedSample, IFormFile uploadedPicture)
        {
            // This method takes in all the possible data from the form and apply it to the Errand. The method is flexible so that fields can be left ununsed.
            Errand errand = repository.GetErrand(errandId).Result;

            if(statusId != null)
            {
                errand.StatusId = statusId;
            }
            if (investigatorAction != null)
            {
                //Newlines don't seem to render properly in HTML if they are inserted here. Instead, add %%% between messages and use it as a delimiter in a split in the view.
                errand.InvestigatorAction = errand.InvestigatorAction + "%%%" + investigatorAction;
            }
            if (investigatorInfo != null)
            {
                //Newlines don't seem to render properly in HTML if they are inserted here. Instead, add %%% between messages and use it as a delimiter in a split in the view.
                errand.InvestigatorInfo = errand.InvestigatorInfo + "%%%" + investigatorInfo;
            }

            // Verify that the file format is pdf
            if (uploadedSample != null && uploadedSample.FileName.EndsWith(".pdf"))
            {
                var tempPath = Path.GetTempFileName();
                using (var stream = new FileStream(tempPath, FileMode.Create))
                {
                    await uploadedSample.CopyToAsync(stream);
                }

                // Define new Sample object
                Sample sample = new Sample();
                sample.ErrandId = errand.ErrandId;
                // Put a counter in the filename to avoid doubles

                string uniqueFilename = "sample" + Guid.NewGuid().ToString() + "_" + uploadedSample.FileName;
                sample.SampleName = uniqueFilename;

                //Upload to temporary folder
                var path = Path.Combine(environment.WebRootPath, "UploadedSamples", uniqueFilename);

                // Move the file
                System.IO.File.Move(tempPath, path);

                // The Sample is added to the Errand
                errand.Samples.Add(sample);
            }
            // Verify that the file format is jpg, jpeg or png
            if (uploadedPicture != null && (uploadedPicture.FileName.EndsWith(".jpg") ||uploadedPicture.FileName.EndsWith(".jpeg") || uploadedPicture.FileName.EndsWith(".png")))
            {


                var tempPath = Path.GetTempFileName();
                using (var stream = new FileStream(tempPath, FileMode.Create))
                {
                    await uploadedPicture.CopyToAsync(stream);
                }

                // Define new Picture object
                Picture picture = new Picture();
                picture.ErrandId = errand.ErrandId;
                // Put a counter in the filename to avoid doubles

                string uniqueFilename = "picture" + Guid.NewGuid().ToString() + "_" + uploadedPicture.FileName;
                picture.PictureName = uniqueFilename;

                //Upload to temporary folder
                var path = Path.Combine(environment.WebRootPath, "UploadedPictures", uniqueFilename);

                // Move the file
                System.IO.File.Move(tempPath, path);

                // The Picture is added to the Errand
                errand.Pictures.Add(picture);
            }

            errand.StatusId = statusId;
            repository.SaveErrand(errand);

            // Use TempData created in the LoggedIn_Crime method use it as a parameter to return to the same page
            return RedirectToAction("LoggedIn_Crime_Investigator", new { id = TempData["id"] });
        }

        public async Task<IActionResult>UploadToRoot(IFormFile uploadedPicture, string folder)
        {
            var tempPath = Path.GetTempFileName();
            if(uploadedPicture.Length > 0)
            {
                // If the file has a non-zero file size
                using (var stream = new FileStream(tempPath, FileMode.Create))
                {
                    // Using
                    await uploadedPicture.CopyToAsync(stream);

                }
            }
            // The following points to the folder under wwwroot
            var path = Path.Combine(environment.WebRootPath,folder,uploadedPicture.FileName);

            System.IO.File.Move(tempPath, path);

            ViewBag.FileName = uploadedPicture.FileName;
            ViewBag.Path = path;
            return RedirectToAction("LoggedIn_Crime_Investigator", new { id = TempData["id"] });
        }
    }
}
