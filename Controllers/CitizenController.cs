﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace EnviroC.Controllers
{
    public class CitizenController : Controller
    {
        private IErrandsRepository repository;


        public CitizenController(IErrandsRepository repo)
        {
            repository = repo;

        }
        public ViewResult Index()
        {
            ViewBag.Title = "Småstads kommun";
            Errand errand = new Errand();

            // Deserialize the saved Json object
            try
            {
                  errand = JsonConvert.DeserializeObject<Errand>(TempData["eTemp"].ToString());
            }
            catch
            {
                Console.WriteLine("Deserialization failed");
            }

            // Uses a view from the Home folder.
            return View("../Home/Index", errand);
        }
        public ViewResult Services()
        {
            ViewBag.Title = "Småstads kommun - Tjänster";
            return View();
        }
        public ViewResult FAQ()
        {
            ViewBag.Title = "Småstads kommun - FAQ";
            return View();
        }
        public ViewResult Contact()
        {
            ViewBag.Title = "Småstads kommun - Kontakt";
            return View();
        }
        public ViewResult Validate(Errand errand)
        {
            ViewBag.Title = "Småstads kommun - Bekräfta";


            // Serialize the Errand object, turning it into a JSON string. This is done because TempData only allows simple data types.
            string eAsJason = JsonConvert.SerializeObject(errand, Formatting.Indented);
            TempData["eTemp"] = eAsJason;

            return View(errand);
        }
        public ViewResult Thanks()
        {
            ViewBag.Title = "Småstads kommun - Rapportera brott";
            Errand errand = new Errand();
            errand.Pictures = new Collection<Picture>();

            // Deserialize the saved Json object
            try
            {
                errand = JsonConvert.DeserializeObject<Errand>(TempData["eTemp"].ToString());
            }
            catch
            {
                Console.WriteLine("Deserialization failed");

            }

            repository.SaveErrand(errand);

            // The object is also sent to Thanks.cshtml so that Model.RefNumber can be displayed
            return View(errand);
        }
    }
}
