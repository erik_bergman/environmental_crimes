﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.Interfaces;
using EnviroC.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EnviroC.Controllers
{
    public class ManagerController : Controller
    {
        private IErrandsRepository repository;


        public ManagerController(IErrandsRepository repo)
        {
            repository = repo;

        }
        [Authorize(Roles = "Manager")]
        public ViewResult LoggedIn_Manager()
        {
            ViewBag.Title = "Småstads kommun - Chef";

            string username = TempData.Peek("username").ToString();
            IQueryable relevantErrands = null;

            Employee thisEmployee = repository.Employees.Where(mng => mng.EmployeeId == this.User.Identity.Name).FirstOrDefault();
            Department thisDepartment = repository.Departments.Where(dep => dep.DepartmentId == thisEmployee.DepartmentId).FirstOrDefault();
            IEnumerable<Employee> relevantEmployees = repository.Employees.Where(emps => emps.DepartmentId == thisDepartment.DepartmentId);
            ViewBag.Title = "Småstads kommun - Handläggare";

            relevantErrands = from err in repository.Errands
                             where err.DepartmentId == thisDepartment.DepartmentId
                             join stat in repository.ErrandStatus1 on err.StatusId equals stat.StatusId
                             join dep in repository.Departments on err.DepartmentId equals dep.DepartmentId into departmentErrand
                             from deptE in departmentErrand.DefaultIfEmpty()
                             join em in repository.Employees on err.EmployeeId equals em.EmployeeId into employeeErrand
                             from empE in employeeErrand.DefaultIfEmpty()
                             orderby err.RefNumber descending
                             select new ViewModel_MyErrand
                             {
                                 DateOfObservation = err.DateOfObservation,
                                 ErrandId = err.ErrandId,
                                 RefNumber = err.RefNumber,
                                 TypeOfCrime = err.TypeOfCrime,
                                 StatusName = stat.StatusName,
                                 DepartmentName = (err.DepartmentId == null ? "Ej tillsatt" : deptE.DepartmentName),
                                 EmployeeName = (err.EmployeeId == null ? "Ej tillsatt" : empE.EmployeeName)
                             };

            ViewBag.thisDepartment = thisDepartment;
            ViewBag.thisEmployee = thisEmployee;
            ViewBag.relevantDepartments = repository.Departments;
            ViewBag.relevantEmployees = relevantEmployees;
            ViewBag.relevantErrandStatus1 = repository.ErrandStatus1;
            return View(relevantErrands);

        }
        [Authorize(Roles = "Manager")]
        public ViewResult LoggedIn_Crime_Manager(int id)
        {
            // This method is called from several other classes
            ViewBag.Title = $"Småstads kommun - Ärende {id}";
            ViewBag.Id = id; //Very important, so that the correct input parameter is handed to InvokeAsync() in ShowOneErrand.cs
            TempData["id"] = id;
            Employee thisManager = repository.Employees.Where(mng => mng.EmployeeId == this.User.Identity.Name).FirstOrDefault();
            Department thisDepartment = repository.Departments.Where(dep => dep.DepartmentId == thisManager.DepartmentId).FirstOrDefault();
            IEnumerable<Employee> relevantEmployees = repository.Employees.Where(emps => emps.DepartmentId == thisDepartment.DepartmentId);
            ViewBag.relevantEmployees = relevantEmployees;
            return View();
        }

        public IActionResult UpdateEmployee(int errandId, string employeeId, bool noAction, string comment)
        {
            Errand errand = repository.GetErrand(errandId).Result;
            if (noAction)
            {
                // Set employee to blank, set status to no action, set info to comment
                errand.EmployeeId = "";
                errand.StatusId = "S_B";
                errand.InvestigatorInfo = comment;
            }
            else
            {
                errand.EmployeeId = employeeId;
            }
            repository.SaveErrand(errand);
            // Use TempData created in the LoggedIn_Crime method use it as a parameter to return to the same page
            return RedirectToAction("LoggedIn_Crime_Manager", new { id = TempData["id"] });
        }
    }
}
