﻿using EnviroC.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.ViewModels;

namespace EnviroC.Components
{
    public class ShowErrandTable : ViewComponent
    {
        private IErrandsRepository repository;
        public ShowErrandTable(IErrandsRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke(IQueryable<ViewModel_MyErrand> myErrandList)
        {

            return View(myErrandList);

        }
    }
}
