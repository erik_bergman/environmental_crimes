﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using EnviroC.Models.Interfaces;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;

namespace EnviroC.Components
{
    public class ShowOneErrand : ViewComponent
    {
        //This class is called by each of the three LoggedIn_Crime pages:
        //@await Component.InvokeAsync("ShowOneErrand", ViewBag.Id)

        private IErrandsRepository repository;
        public ShowOneErrand(IErrandsRepository repo)
        {
            // This repository is now of the type FakeErrandsRepository beacuse of services.AddTransient<IErrandsRepository, FakeErrandsRepository>() in Startup.cs.
            repository = repo;
        }

        public async Task<IViewComponentResult> InvokeAsync(int id){

            //GetErrand() is part of EFDataRepository.cs.
            var errandDetail = await repository.GetErrand(id);
            return View(errandDetail);

        }
    }
}
