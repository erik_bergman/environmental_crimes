using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnviroC.Models;
using EnviroC.Models.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Identity;
using EnviroC.Migrations;

namespace EnviroC
{
    public class Startup
    {
        // The IConfiguration object is used as an input parameter in UseSqlServer in order to connect the webpage to the settings in appsettings.json
        public IConfiguration Configuration {get;}
        public Startup(IConfiguration config) => Configuration = config;

        public void ConfigureServices(IServiceCollection services)
        {
            // The following two connection strings are defined in appsettings.json
            services.AddDbContext<AppIdentityDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("IdentityConnection")));
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<AppIdentityDbContext>();  
            
            // Defines the default page when unable to reach a page that requires login
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Home/Login";
            });

            // This transient is used to connect to the data that is present by default. Previously, the transient connected to FakeErrandsRepository.cs.  
            services.AddTransient<IErrandsRepository, EFDataRepository>();
            services.AddControllersWithViews();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                // The below route points at controller=Home, action=Index, id=nothing
                endpoints.MapDefaultControllerRoute();

            });
        }
    }
}
